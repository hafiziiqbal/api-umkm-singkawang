@extends('layouts.mainedit')

@section('container')
    <div style="margin-left:15%; margin-right:15%">
        @if ($errors->any())
            <div style="width:100%" class="alert alert-danger">
                <p><strong>Opps Ada Yang Harus Diperbaiki</strong></p>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <a href="{{ route('usaha.index') }}"><button class="btn btn-secondary">
                << Back</button></a>

        <h2 class="fw-bold mb-5 text-center">
            Memperbaru Data UMKM
        </h2>

        <h3 class="mb-4">
            A. Data Pemilik Usaha
        </h3>

        {{-- NIK --}}
        <div class="row align-items-start">
            <div class="p-2 mb-5 mt-3" style="width:35%;">
                <label style="font-size: 17px">Nomor Induk Kependudukan (NIK)</label>
            </div>
            <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                :
            </div>
            <div class="col">
                <div class="mb-5 mt-3">
                    <input type="number" pattern="[0-9.]+" class="form-control p-2" aria-describedby="emailHelp" name="nik"
                        value="{{ $relasiUsahaById[0]->user->nik }}" required readonly>
                </div>
            </div>
        </div>

        {{-- Nama Pemilik Usaha --}}
        <div class="row align-items-start">
            <div class="p-2 mb-5 mt-3" style="width:35%;">
                <label style="font-size: 17px">Nama Pemilik Usaha (sesuai KTP)</label>
            </div>
            <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                :
            </div>
            <div class="col">
                <div class="mb-5 mt-3">
                    <input type="text" class="form-control p-2" name="nama" required
                        value="{{ $relasiUsahaById[0]->user->nama }}" readonly>
                </div>
            </div>
        </div>

        {{-- Jenis Kelamin --}}
        <div class="row align-items-start">
            <div class="p-2 mb-5 mt-3" style="width:35%;">
                <label style="font-size: 17px">Jenis Kelamin</label>
            </div>
            <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                :
            </div>
            <div class="col">
                <div class="mb-5 mt-4 d-flex">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="jenis_kelamin" id="flexRadioDefault1"
                            value="Laki-Laki"
                            {{ $relasiUsahaById[0]->user->jenis_kelamin == 'Laki-Laki' ? 'checked' : 'disabled' }}
                            readonly>
                        <label class="form-check-label" for="flexRadioDefault1">
                            Laki-Laki
                        </label>
                    </div>
                    &emsp;
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="jenis_kelamin" id="flexRadioDefault2"
                            value="Perempuan"
                            {{ $relasiUsahaById[0]->user->jenis_kelamin == 'Perempuan' ? 'checked' : 'disabled' }}
                            readonly>
                        <label class="form-check-label" for="flexRadioDefault2">
                            Perempuan
                        </label>
                    </div>
                </div>
            </div>
        </div>

        {{-- Alamat --}}
        <div class="row align-items-start">
            <div class="p-2 mb-5 mt-3" style="width:35%;">
                <label style="font-size: 17px">Alamat Tempat Tinggal</label>
            </div>
            <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                :
            </div>
            <div class="col">
                <div class="mb-5 mt-3">
                    <textarea class="form-control" name="alamat" id="floatingTextarea2" style="height: 150px" required
                        readonly>{{ $relasiUsahaById[0]->user->alamat }}</textarea>
                </div>
            </div>
        </div>

        {{-- No Hp --}}
        <div class="row align-items-start">
            <div class="p-2 mb-5 mt-3" style="width:35%;">
                <label style="font-size: 17px">No. Handphone</label>
            </div>
            <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                :
            </div>
            <div class="col">
                <div class="mb-5 mt-3">
                    <input type="number" pattern="[0-9.]+" class="form-control p-2" name="no_hp"
                        value="{{ $relasiUsahaById[0]->user->no_hp }}" required readonly>
                </div>
            </div>
        </div>

        <form action="{{ route('usaha.update', $relasiUsahaById[0]->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')

            <input type="hidden" name="id" value="{{ $relasiUsahaById[0]->user->id }}">


            <h3 class="mb-4">
                B. Data Usaha
            </h3>

            {{-- Nama Usaha --}}
            <div class="row align-items-start">
                <div class="p-2 mb-5 mt-3" style="width:35%;">
                    <label style="font-size: 17px">Nama Usaha</label>
                </div>
                <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                    :
                </div>
                <div class="col">
                    <div class="mb-5 mt-3">
                        <input type="text" class="form-control p-2" name="nama_usaha"
                            value="{{ $relasiUsahaById[0]->nama }}">
                    </div>
                </div>
            </div>

            {{-- NIB --}}
            <div class="row align-items-start">
                <div class="p-2 mb-5 mt-3" style="width:35%;">
                    <label style="font-size: 17px">NIB</label>
                </div>
                <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                    :
                </div>
                <div class="col">
                    <div class="mb-5 mt-3">
                        <input type="number" pattern="[0-9.]+" class="form-control p-2" name="nib"
                            value="{{ $relasiUsahaById[0]->nib }}">
                    </div>
                </div>
            </div>

            {{-- Produk / Jenis Usaha --}}
            <div class="row align-items-start">
                <div class="p-2 mb-5 mt-3" style="width:35%;">
                    <label style="font-size: 17px">Produk / Jenis Usaha</label>
                </div>
                <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                    :
                </div>
                <div class="col">
                    <div class="mb-5 mt-3">
                        <input type="text" class="form-control p-2" name="jenis_usaha"
                            value="{{ $relasiUsahaById[0]->jenis }}">
                    </div>
                </div>
            </div>

            {{-- Perizinan --}}
            <div class="row align-items-start">
                <div class="p-2 mb-5 mt-3" style="width:35%;">
                    <label style="font-size: 17px">Perizinan yang dimilik & no/tgl</label>
                </div>
                <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                    :
                </div>
                <div class="col">
                    <div class="mb-5 mt-3">
                        <div class="izin-card">
                            <input type="hidden" id="number" value="{{ count($relasiUsahaById[0]->perizinan) - 1 }}">
                            @foreach ($relasiUsahaById[0]->perizinan as $key => $izin)
                                <div id="{{ $key != 0 ? 'izin' . $key : '' }}" class="card p-2 mb-4">
                                    <input type="hidden" id="numberizin" value="{{ $key }}">
                                    <button class="close-izin btn-close" {{ $key != 0 ? '' : 'hidden' }} type="button"
                                        aria-label="Close" onclick="deleteCard()"></button>
                                    <input type="number" pattern="[0-9.]+" class="form-control p-2 mb-3"
                                        name="perizinan[{{ $key }}][kbli_id]" placeholder="KBLI"
                                        value="{{ $izin->kbli_id }}">
                                    <div class="d-flex">
                                        <input type="date" class=" p-2 me-3"
                                            name="perizinan[{{ $key }}][tanggal]" value="{{ $izin->tanggal }}">
                                        <input type="text" class="form-control p-2"
                                            name="perizinan[{{ $key }}][iumk]" placeholder="IUMK No."
                                            value="{{ $izin->iumk_nomor }}">
                                    </div>

                                </div>
                            @endforeach
                        </div>


                        <div>
                            <button type="button" class="btn btn-secondary" style="float: right"
                                onClick="addIzinCard()">Tambah
                                Perizinan</button>
                        </div>

                    </div>
                </div>
            </div>

            {{-- Jenis Badan Usaha --}}
            <div class="row align-items-start">
                <div class="p-2 mb-5 mt-3" style="width:35%;">
                    <label style="font-size: 17px">Jenis Badan Usaha</label>
                </div>
                <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                    :
                </div>
                <div class="col">
                    <div class="mb-5 mt-3">
                        <select class="form-select" aria-label="Default select example" name="jenis_badan_usaha">
                            @foreach ($daftarJenisBadanUsaha as $jenisBadanUsaha)
                                <option
                                    {{ $relasiUsahaById[0]->jenis_badan_usaha_id == $jenisBadanUsaha->id ? 'selected="selected"' : '' }}
                                    value="{{ $jenisBadanUsaha->id }}">{{ $jenisBadanUsaha->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            {{-- Alamat Tempat Usaha --}}
            <div class="row align-items-start">
                <div class="p-2 mb-5 mt-3" style="width:35%;">
                    <label style="font-size: 17px">Alamat Tempat Usaha</label>
                </div>
                <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                    :
                </div>
                <div class="col">
                    <div class="mb-5 mt-3">
                        <textarea class="form-control" name="alamat_usaha" id="floatingTextarea2"
                            style="height: 150px">{{ $relasiUsahaById[0]->alamat }}</textarea>
                    </div>
                </div>
            </div>

            {{-- Asset --}}
            <div class="row align-items-start">
                <div class="p-2 mb-5 mt-3" style="width:35%;">
                    <label style="font-size: 17px">Asset</label>
                </div>
                <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                    :
                </div>
                <div class="col">
                    <div class="mb-5 mt-3">
                        <input type="number" pattern="[0-9.]+" class="form-control p-2" name="aset"
                            value="{{ $relasiUsahaById[0]->aset }}">
                    </div>
                </div>
            </div>

            {{-- Rata Omset Perbulan --}}
            <div class="row align-items-start">
                <div class="p-2 mb-5 mt-3" style="width:35%;">
                    <label style="font-size: 17px">Omset Rata - rata per Bulan</label>
                </div>
                <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                    :
                </div>
                <div class="col">
                    <div class="mb-5 mt-3">
                        <input type="number" pattern="[0-9.]+" class="form-control p-2" name="rata_omset_perbulan"
                            value="{{ $relasiUsahaById[0]->rata_omset_perbulan }}">
                    </div>
                </div>
            </div>

            {{-- Jumlah Karyawan --}}
            <div class="row align-items-start">
                <div class="p-2 mb-5 mt-3" style="width:35%;">
                    <label style="font-size: 17px">Jumlah Karyawan</label>
                </div>
                <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                    :
                </div>
                <div class="col">
                    <div class="mb-5 mt-3 d-flex">
                        <input type="numeric" class="form-control p-2 me-3" placeholder="Laki-Laki" name="karyawan_lk"
                            value="{{ $relasiUsahaById[0]->karyawan_lk }}">
                        <input type="numeric" class="form-control p-2" placeholder="Perempuan" name="karyawan_pr"
                            value="{{ $relasiUsahaById[0]->karyawan_pr }}">
                    </div>
                </div>
            </div>

            {{-- Foto Logo --}}
            <div class="row align-items-start">
                <div class="p-2 mb-4" style="width: 50%">
                    <label for="formFile" class="text-secondary mb-2">Foto Logo Usaha</label>
                    <div class="cardlogo">
                        <div id="card-logo">
                            <button type="button" class="close-izin btn-close" aria-label="Close"
                                onclick="deleteCardLogo()"></button><br>

                            <img src="{{ getenv('LINK_IMAGE') . $logo }}" width="150rem" alt="">
                        </div>
                    </div>

                    {{-- <input class="form-control" type="file" id="formFile" name="logo" required> --}}
                </div>
            </div>

            {{-- Foto Produk --}}
            <div class="row align-items-start">
                <div class="p-2 mb-4">
                    <label for="formFile" class="text-secondary mb-2">Foto Produk</label>
                    <br>
                    <input type="hidden" id="numberproduk" value="0">
                    <button onClick="addImg('.card-produk','produk','numberproduk')" type="button"
                        class="btn btn-secondary p-2 mb-3">+
                        Produk</button>
                    <div class="pathproduk"></div>
                    <div class="cardproduk card-produk d-flex">

                        @foreach ($produk as $key => $produk)
                            @if ($produk != '')
                                <div id="card-produk{{ $key }}" class="me-3">
                                    <input type="hidden" id="pathproduk{{ $key }}" value="{{ $produk->id }}">
                                    <button type="button" class="close-izin btn-close" aria-label="Close"
                                        onClick="deleteCardProduk('card-produk{{ $key }}', 'pathproduk{{ $key }}')"></button><br>
                                    <img src="{{ getenv('LINK_IMAGE') . $produk->foto }}" width="150rem" alt="">
                                </div>
                            @endif

                        @endforeach

                    </div>

                </div>
            </div>

            {{-- Foto Lokasi --}}
            <div class="row align-items-start">
                <div class="p-2 mb-4">
                    <label for="formFile" class="text-secondary mb-2">Foto Lokasi</label>
                    <br>
                    <input type="hidden" id="numberlokasi" value="0">
                    <button onClick="addImg('.card-lokasi','lokasi', 'numberlokasi')" type="button"
                        class="btn btn-secondary p-2 mb-3">+
                        Lokasi</button>
                    <div class="pathlokasi"></div>
                    <div class="cardlokasi card-lokasi d-flex">
                        @foreach ($lokasi as $key => $lokasi)
                            @if ($lokasi != '')
                                <div id="card-lokasi{{ $key }}" class="me-3">
                                    <input type="hidden" id="pathlokasi{{ $key }}" value="{{ $lokasi->id }}">
                                    <button type="button" class="close-izin btn-close" aria-label="Close"
                                        onClick="deleteCardLokasi('card-lokasi{{ $key }}', 'pathlokasi{{ $key }}')"></button><br>
                                    <img src="{{ getenv('LINK_IMAGE') . $lokasi->foto }}" width="150rem" alt="">
                                </div>
                            @endif
                        @endforeach
                    </div>

                </div>
            </div>



            <div class="row align-items-start">
                <div class="col- mb-4">
                    <button type="submit" class="btn btn-dark" style="width: 100%">Perbaharui Data UKM</button>
                </div>



            </div>

        </form>






    </div>

@endsection
