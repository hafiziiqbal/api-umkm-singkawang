@extends('layouts.main')

@section('container')
    <div style="margin-right:25%;">
        <h2 class="fw-bold mb-5">
            Dashboard UKM
        </h2>

        <div class="row align-items-start" style="width:100%;">
            {{-- RATA OMSET PERBULAN --}}
            <div class="col-7 mb-5 me-5 p-3 card-dashboard" style=" height:20rem;">
                <h5 class="ms-4 mb-2 fw-bold">Omset perbulan UKM</h5>
                <div style="width: 100%; height:90%">
                    <canvas id="omsetperbulan" height="130">
                    </canvas>
                </div>
            </div>

            {{-- UKM PRIA DAN WANITA --}}
            <div class="col mb-3 p-3 card-dashboard" style="width:30%;  height:20rem;">

                <td>

                    <div class="row align-items-start" style="width: 100%; height:45%">
                        <div class="col-5 relative">

                            <canvas id="ukmpria" height="130">
                            </canvas>
                            <div class="absolute-center text-center">
                                <p style="margin: 0%" class="fs-3 fw-bold">74%</p>
                            </div>
                        </div>
                        <div class="col relative" style="height:100%">
                            <div class="absolute-center-left">
                                <h4 style="margin: 0%" class="fw-bold">UKM Pria</h4>
                                <p style="margin: 0%; color: gray;" class="fs-6">tahun 2021/2022</p>
                            </div>
                        </div>

                    </div>

                </td>
                <br>
                <td>
                    <div class="row align-items-start" style=" width: 100%; height:45%">
                        <div class="col-5 relative">
                            <canvas id="ukmwanita" height="130">
                            </canvas>
                            <div class="absolute-center text-center">
                                <p style="margin: 0%" class="fs-3 fw-bold">26%</p>
                            </div>

                        </div>
                        <div class="col relative" style="height:100%">
                            <div class="absolute-center-left">
                                <h4 style="margin: 0%" class="fw-bold">UKM Wanita</h4>
                                <p style="margin: 0%; color: gray;" class="fs-6">tahun 2021/2022</p>
                            </div>
                        </div>

                    </div>
                </td>



            </div>
        </div>

        <div class="row align-items-start" style="width: 100%">

            <div style="height: 40rem" class="col-4 mb-5 me-4 p-5 card-dashboard">
                <p class="fs-5 fw-bold mb-3">Omset Perbulan <br> UKM Terbesar</p>
                @for ($i = 0; $i < 5; $i++)
                    <div class="card-list p-3 mb-3">
                        <p class="fs-5" style="margin: 0px;">Hafizi Iqbal</p>
                        <p class="label-p">Fizi Water Corporation</p>
                    </div>
                @endfor

            </div>


            <div style="height: 40rem;width: 30%;" class="col mb-5 p-5 card-dashboard">

                <div class="row mb-5">
                    <h5 class="fw-bold">Karyawan Pria</h5>
                    <p class="label-p mb-3" style="font-size: 14px">tahun 2021/2022</p>
                    <div style="width: 100%; height:90%">
                        <div id="progressLk" class="cont-progressbar">
                            <div id="fillProgressLk" style="background-color: black;" class="cont-progressbar">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-5">
                    <h5 class="fw-bold">Karyawan Wanita</h5>
                    <p class="label-p mb-3" style="font-size: 14px">tahun 2021/2022</p>
                    <div style="width: 100%; height:90%">
                        <div id="progressPr" class="cont-progressbar">
                            <div id="fillProgressPr" style="background-color: black;" class="cont-progressbar">
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row mb-3">
                    <h5 class="fw-bold">Jenis-Jenis Badan Usaha</h5>
                    <div style="width: 100%; height:17rem">
                        <canvas id="jenisBadanUsaha" height="130">
                        </canvas>
                    </div>
                </div>
            </div>


        </div>

    </div>

    </div>
    @push('scriptchart')
        <script>
            const omsetperbulan = document.getElementById('omsetperbulan').getContext('2d');
            const myChart = new Chart(omsetperbulan, {
                type: 'bar',
                data: {
                    labels: ['< 100jt', '100 - 200jt', '200 - 300jt', '300 - 500jt'],
                    datasets: [{
                        data: [130, 220, 180, 60],
                        backgroundColor: '#000000',
                        hoverBackgroundColor: '#b4b3b3',
                        borderRadius: 15,
                        barThickness: 45,
                        borderSkipped: false
                    }]
                },
                options: {
                    maintainAspectRatio: false,
                    plugins: {
                        legend: {
                            display: false,
                        },
                    },
                    scales: {
                        y: {
                            grid: {
                                drawBorder: false
                            },
                            beginAtZero: true,
                            ticks: {
                                font: {
                                    size: 12,
                                    family: 'Poppins'
                                }
                            }
                        },
                        x: {
                            grid: {
                                display: false,
                            },
                            ticks: {
                                font: {
                                    size: 12,
                                    family: 'Poppins'
                                }
                            }
                        },
                    }
                }
            });

            const ukmpria = document.getElementById('ukmpria').getContext('2d');
            const ukmpriaChart = new Chart(ukmpria, {
                type: 'doughnut',
                data: {
                    labels: ['UKM PRIA'],
                    datasets: [{
                        data: [74, 26],
                        backgroundColor: ['#000000', '#c2c0c0'],
                        borderRadius: [5, 0],
                        cutout: '80%',
                        borderWidth: 0,
                        rotation: 140
                    }]
                },
                options: {
                    transitions: {
                        active: {
                            animation: {
                                duration: 400
                            }
                        }
                    },
                    maintainAspectRatio: false,
                    plugins: {
                        legend: {
                            display: false,
                        },
                    },
                }
            });
            const ukmwanita = document.getElementById('ukmwanita').getContext('2d');
            const ukmWanitaChart = new Chart(ukmwanita, {
                type: 'doughnut',
                data: {
                    labels: ['UKM WANITA'],
                    datasets: [{
                        data: [26, 74],
                        backgroundColor: ['#000000', '#c2c0c0'],
                        borderRadius: [5, 0],
                        cutout: '80%',
                        borderWidth: 0,
                        rotation: 40
                    }]
                },
                options: {
                    maintainAspectRatio: false,
                    plugins: {
                        legend: {
                            display: false,
                        },
                    },
                }








            });

            function myFunction() {
                var i = 0;
                if (i == 0) {
                    i = 1;
                    var elemLk = document.getElementById('fillProgressLk');
                    var elemPr = document.getElementById('fillProgressPr');
                    var widthLk = 1;
                    var widthPr = 1;
                    var idLk = setInterval(frameLk, 10);
                    var idPr = setInterval(framePr, 10);

                    function frameLk() {
                        if (widthLk >= 74) {
                            clearInterval(idLk);
                            i = 0;
                        } else {
                            widthLk++;
                            elemLk.style.width = widthLk + "%";
                        }
                    }

                    function framePr() {
                        if (widthPr >= 26) {
                            clearInterval(idPr);
                            i = 0;
                        } else {
                            widthPr++;
                            elemPr.style.width = widthPr + "%";
                        }
                    }
                }




            }

            const jenisBadanUsaha = document.getElementById('jenisBadanUsaha').getContext('2d');
            const jenisBadanUsahaChart = new Chart(jenisBadanUsaha, {
                type: 'bar',
                data: {
                    labels: [
                        "PT", "CV", "Koperasi", "Perorangan"
                    ],
                    datasets: [{
                        label: "Jumlah UKM",
                        data: [10, 40, 55, 33],
                        backgroundColor: '#000000',
                        borderRadius: 15,
                        barThickness: 25,
                    }]
                },
                options: {
                    indexAxis: 'y',
                    elements: {
                        bar: {
                            borderWidth: 2,
                        }
                    },

                    maintainAspectRatio: false,
                    plugins: {
                        legend: {
                            display: false
                        }
                    },
                    scales: {
                        y: {
                            grid: {
                                display: false,
                                drawBorder: false
                            },
                            beginAtZero: true,
                            ticks: {
                                padding: 10,
                                crossAlign: "far",
                                align: 'start',
                                font: {
                                    size: 12,
                                    family: 'Poppins'
                                }
                            }
                        },
                        x: {
                            ticks: {
                                font: {
                                    size: 12,
                                    family: 'Poppins',
                                }
                            }
                        },
                    },

                }
            })
        </script>
    @endpush

@endsection
