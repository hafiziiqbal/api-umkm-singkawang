@extends('layouts.main')

@section('container')

    @if (session('message'))
        <div style="width: 70%" class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif
    @if ($errors->any())
        <div style="width:70%" class="alert alert-danger">
            <p><strong>Opps Ada Yang Harus Diperbaiki</strong></p>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <h2 class="fw-bold mb-5">
        Pendaftaran UMKM Kota Singkawang
    </h2>

    <h3 class="mb-4">
        A. Data Pemilik Usaha
    </h3>

    <form action="{{ route('usaha.store') }}" method="POST" enctype="multipart/form-data">
        @csrf

        {{-- NIK --}}
        <div class="row align-items-start" style="width:70%">
            <div class="p-2 mb-5 mt-3" style="width:35%;">
                <label style="font-size: 17px">Nomor Induk Kependudukan (NIK)</label>
            </div>
            <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                :
            </div>
            <div class="col">
                <div class="mb-5 mt-3">
                    <input type="number" pattern="[0-9.]+" class="form-control p-2" aria-describedby="emailHelp" name="nik"
                        required>
                </div>
            </div>
        </div>

        {{-- Nama Pemilik Usaha --}}
        <div class="row align-items-start" style="width:70%">
            <div class="p-2 mb-5 mt-3" style="width:35%;">
                <label style="font-size: 17px">Nama Pemilik Usaha (sesuai KTP)</label>
            </div>
            <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                :
            </div>
            <div class="col">
                <div class="mb-5 mt-3">
                    <input type="text" class="form-control p-2" name="nama" required>
                </div>
            </div>
        </div>

        {{-- Jenis Kelamin --}}
        <div class="row align-items-start" style="width:70%">
            <div class="p-2 mb-5 mt-3" style="width:35%;">
                <label style="font-size: 17px">Jenis Kelamin</label>
            </div>
            <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                :
            </div>
            <div class="col">
                <div class="mb-5 mt-4 d-flex">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="jenis_kelamin" id="flexRadioDefault1"
                            value="Laki-Laki" checked>
                        <label class="form-check-label" for="flexRadioDefault1">
                            Laki-Laki
                        </label>
                    </div>
                    &emsp;
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="jenis_kelamin" id="flexRadioDefault2"
                            value="Perempuan">
                        <label class="form-check-label" for="flexRadioDefault2">
                            Perempuan
                        </label>
                    </div>
                </div>
            </div>
        </div>

        {{-- Alamat --}}
        <div class="row align-items-start" style="width:70%">
            <div class="p-2 mb-5 mt-3" style="width:35%;">
                <label style="font-size: 17px">Alamat Tempat Tinggal</label>
            </div>
            <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                :
            </div>
            <div class="col">
                <div class="mb-5 mt-3">
                    <textarea class="form-control" name="alamat" id="floatingTextarea2" style="height: 150px"
                        required></textarea>
                </div>
            </div>
        </div>

        {{-- No Hp --}}
        <div class="row align-items-start" style="width:70%">
            <div class="p-2 mb-5 mt-3" style="width:35%;">
                <label style="font-size: 17px">No. Handphone</label>
            </div>
            <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                :
            </div>
            <div class="col">
                <div class="mb-5 mt-3">
                    <input type="number" pattern="[0-9.]+" class="form-control p-2" name="no_hp" required>
                </div>
            </div>
        </div>

        <h3 class="mb-4">
            B. Data Usaha
        </h3>

        {{-- Nama Usaha --}}
        <div class="row align-items-start" style="width:70%">
            <div class="p-2 mb-5 mt-3" style="width:35%;">
                <label style="font-size: 17px">Nama Usaha</label>
            </div>
            <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                :
            </div>
            <div class="col">
                <div class="mb-5 mt-3">
                    <input type="text" class="form-control p-2" name="nama_usaha">
                </div>
            </div>
        </div>

        {{-- NIB --}}
        <div class="row align-items-start" style="width:70%">
            <div class="p-2 mb-5 mt-3" style="width:35%;">
                <label style="font-size: 17px">NIB</label>
            </div>
            <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                :
            </div>
            <div class="col">
                <div class="mb-5 mt-3">
                    <input type="number" pattern="[0-9.]+" class="form-control p-2" name="nib">
                </div>
            </div>
        </div>

        {{-- Produk / Jenis Usaha --}}
        <div class="row align-items-start" style="width:70%">
            <div class="p-2 mb-5 mt-3" style="width:35%;">
                <label style="font-size: 17px">Produk / Jenis Usaha</label>
            </div>
            <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                :
            </div>
            <div class="col">
                <div class="mb-5 mt-3">
                    <input type="text" class="form-control p-2" name="jenis_usaha">
                </div>
            </div>
        </div>

        {{-- KBLI --}}
        <div class="row align-items-start" style="width:70%">
            <div class="p-2 mb-5 mt-3" style="width:35%;">
                <label style="font-size: 17px">KBLI</label>
            </div>
            <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                :
            </div>
            <div class="col">
                <div class="mb-5 mt-3">

                    <div class="kbli-card">

                        <input type="hidden" id="number" value="0">
                        <div id="select" class="card p-2 mb-4">
                            <select class="selectKbli form-select" aria-label="Default select example"
                                name="kbli[0][kbli_id]">
                                @foreach ($listKbli as $kbli)
                                    <option value="{{ $kbli->id }}">
                                        {{ $kbli->no_kbli . ' ' . \Illuminate\Support\Str::limit($kbli->nama, 50) }}
                                    </option>
                                @endforeach
                            </select>
                            {{-- <input type="number" pattern="[0-9.]+" class="form-control p-2 mb-3" name="kbli[0][no_kbli]"
                                placeholder="NOMOR KBLI"> --}}
                            <div class="d-flex">
                                <input type="date" class="form-control p-2 me-3" name="kbli[0][tanggal]">
                                <input type="text" class="form-control p-2" name="kbli[0][iumk]" placeholder="IUMK No.">
                            </div>
                        </div>
                    </div>
                    <div>
                        <button type="button" class="btn btn-secondary" style="float: right" onClick="addKbliCard()">Tambah
                            Perizinan</button>
                    </div>

                </div>
            </div>
        </div>

        {{-- IZIN --}}
        <div class="row align-items-start" style="width:70%">
            <div class="p-2 mb-5 mt-3" style="width:35%;">
                <label style="font-size: 17px">IZIN</label>
            </div>
            <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                :
            </div>
            <div class="col">
                <div class="mb-5 mt-3">

                    <div class="kbli-card">
                        <input type="hidden" id="numberIzin" value="0">
                        <div class="card p-2 mb-4">
                            <input type="number" pattern="[0-9.]+" class="form-control p-2 mb-3" name="kbli[0][no_kbli]"
                                placeholder="NOMOR KBLI">
                            <div class="d-flex">
                                <input type="date" class="form-control p-2 me-3" name="kbli[0][tanggal]">
                                <input type="text" class="form-control p-2" name="kbli[0][iumk]" placeholder="IUMK No.">
                            </div>
                        </div>
                    </div>
                    <div>
                        <button type="button" class="btn btn-secondary" style="float: right" onClick="addKbliCard()">Tambah
                            Perizinan</button>
                    </div>

                </div>
            </div>
        </div>

        {{-- Jenis Badan Usaha --}}
        <div class="row align-items-start" style="width:70%">
            <div class="p-2 mb-5 mt-3" style="width:35%;">
                <label style="font-size: 17px">Jenis Badan Usaha</label>
            </div>
            <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                :
            </div>
            <div class="col">
                <div class="mb-5 mt-3">
                    <select class="form-select" aria-label="Default select example" name="jenis_badan_usaha">
                        @foreach ($daftarJenisBadanUsaha as $jenisBadanUsaha)
                            <option value="{{ $jenisBadanUsaha->id }}">{{ $jenisBadanUsaha->nama }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        {{-- Alamat Tempat Usaha --}}
        <div class="row align-items-start" style="width:70%">
            <div class="p-2 mb-5 mt-3" style="width:35%;">
                <label style="font-size: 17px">Alamat Tempat Usaha</label>
            </div>
            <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                :
            </div>
            <div class="col">
                <div class="mb-5 mt-3">
                    <textarea class="form-control" name="alamat_usaha" id="floatingTextarea2"
                        style="height: 150px"></textarea>
                </div>
            </div>
        </div>

        {{-- Asset --}}
        <div class="row align-items-start" style="width:70%">
            <div class="p-2 mb-5 mt-3" style="width:35%;">
                <label style="font-size: 17px">Asset</label>
            </div>
            <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                :
            </div>
            <div class="col">
                <div class="mb-5 mt-3">
                    <input type="number" pattern="[0-9.]+" class="form-control p-2" name="aset">
                </div>
            </div>
        </div>

        {{-- Rata Omset Perbulan --}}
        <div class="row align-items-start" style="width:70%">
            <div class="p-2 mb-5 mt-3" style="width:35%;">
                <label style="font-size: 17px">Omset Rata - rata per Bulan</label>
            </div>
            <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                :
            </div>
            <div class="col">
                <div class="mb-5 mt-3">
                    <input type="number" pattern="[0-9.]+" class="form-control p-2" name="rata_omset_perbulan">
                </div>
            </div>
        </div>

        {{-- Jumlah Karyawan --}}
        <div class="row align-items-start" style="width:70%">
            <div class="p-2 mb-5 mt-3" style="width:35%;">
                <label style="font-size: 17px">Jumlah Karyawan</label>
            </div>
            <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                :
            </div>
            <div class="col">
                <div class="mb-5 mt-3 d-flex">
                    <input type="numeric" class="form-control p-2 me-3" placeholder="Laki-Laki" name="karyawan_lk">
                    <input type="numeric" class="form-control p-2" placeholder="Perempuan" name="karyawan_pr">
                </div>
            </div>
        </div>


        {{-- Foto Logo --}}
        <div class="row align-items-start">
            <div class="col-3 p-2 mb-4">
                <label for="formFile" class="text-secondary mb-2">Foto Logo Usaha</label>
                <input class="form-control" type="file" id="formFile" name="logo" required>
            </div>
        </div>

        {{-- Foto Produk --}}
        <div class="row align-items-start">
            <div class="p-2 mb-4" style="width: 60%">
                <label for="formFile" class="text-secondary mb-2">Foto Produk</label>
                <br>
                <input type="hidden" id="numberproduk" value="0">
                <input type="hidden" id="number" value="0">
                <button onClick="addImg('.card-produk','produk', 'number')" type="button"
                    class="btn btn-secondary p-2 mb-3">+
                    Produk</button>
                <div class="card-produk d-flex">
                    <div class="card me-3">
                        <div class="card-body mt-4">
                            <input type="file" class="form-control form-control-sm" accept="image/*" name="produk[0]"
                                required>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- Foto Lokasi --}}
        <div class="row align-items-start">
            <div class="p-2 mb-4" style="width: 60%">
                <label for="formFile" class="text-secondary mb-2">Foto Lokasi</label>
                <br>
                <input type="hidden" id="numberlokasi" value="0">
                <input type="hidden" id="number" value="0">
                <button onClick="addImg('.card-lokasi','lokasi', 'number')" type="button"
                    class="btn btn-secondary p-2 mb-3">+
                    Lokasi</button>
                <div class="card-lokasi d-flex">

                    <div class="card me-3">
                        <div class="card-body mt-4">
                            <input type="file" class="form-control form-control-sm" accept="image/*" name="lokasi[0]"
                                required>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row align-items-start">
            <div class="col- mb-4">
                <button type="submit" class="btn btn-dark" style="width: 70%">SIMPAN UKM BARU</button>
            </div>



        </div>
    </form>
@endsection

@push('select')
    <script>
        $('#select').ready(function() {
            $('.selectKbli').select2();
        });
    </script>
@endpush
