<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>CodePen - 403 Forbidden</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/style403.css') }}">

</head>

<body>
    <!-- partial:index.partial.html -->
    <div class="lock"></div>
    <div class="message">
        <h1>Halaman Ini Hanya Bisa Diakses Oleh Pendata</h1>
        <a href="{{ route('usaha.create') }}"><button style="width:30%; margin:0px 35%"
                class="btn btn-dark p-3">KEMBALI</button></a>

    </div>
    <!-- partial -->

</body>

</html>
