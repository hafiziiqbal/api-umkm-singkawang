@extends('layouts.main')

@section('container')
    <div style="margin-right:30%">
        @if (session('messageError'))
            <div style="width: 100%" class="alert alert-danger">{{ session('messageError') }}</div>
        @endif

        @if (session('sendFail'))
            <div style="width: 100%" class="alert alert-danger">
                <p><strong>Opps Pesan Gagal Dikirim Ke :</strong></p>
                <ul>
                    @foreach (session('sendFail') as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>

        @endif

        @if (session('message'))
            <div style="width: 100%" class="alert alert-success">{{ session('message') }}</div>
        @endif

        <h2 class="fw-bold mb-5">
            Kirim Pesan Masal
        </h2>

        <form action="{{ route('sms.write') }}" method="post">
            @csrf
            <button style="float: right; width:10rem;" type="submit" class="btn btn-dark p-2">Kirim
                Pesan</button>
            <br><br><br>
            <table class="table table-striped " id="usahatable">
                <thead>
                    <tr>
                        <th scope="col">NO</th>
                        <th scope="col">Nama Pemilik Usaha</th>
                        <th scope="col">Nama Usaha</th>
                        <th scope="col">Pilih</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($daftarUser as $key => $user)
                        <tr>
                            <th scope="row">{{ $key + 1 }}</th>
                            <td>{{ $user->nama }}</td>
                            <td>{{ !empty($user->usaha) ? $user->usaha->nama : '' }}</td>
                            <td>
                                <input class="form-check-input userId" type="checkbox" id="user-Id" name="userid[]"
                                    value={{ $user->id }}>
                            </td>
                        </tr>

                    @endforeach
                </tbody>
        </form>


        </table>


    </div>
    @push('listUkm')
        <script>
            $(document).ready(function() {
                $('#usahatable').DataTable();
            });
        </script>
    @endpush

@endsection
