@extends('layouts.main')

@section('container')

    <div style="margin-right:25%">
        @if (session('message'))
            <div style="width: 100%" class="alert alert-success">{{ session('message') }}</div>
        @endif

        @if (session('messageError'))
            <div style="width: 100%" class="alert alert-danger">{{ session('messageError') }}</div>
        @endif

        <h2 class="fw-bold mb-5">
            List Pengusaha UKM Singkawang
        </h2>

        <table class="table table-striped" id="usahatable">
            <thead>
                <tr>
                    <th scope="col">NO</th>
                    <th scope="col">NIK</th>
                    <th scope="col">Nama Pengusaha</th>
                    <th scope="col">Jenis Kelamin</th>
                    <th scope="col">No. HP</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($daftarPengusaha as $key => $pengusaha)
                    <tr>
                        <th scope="row">{{ $key + 1 }}</th>
                        <td>{{ $pengusaha->nik }}</td>
                        <td>{{ $pengusaha->nama }}</td>
                        <td>{{ $pengusaha->jenis_kelamin }}</td>
                        <td>{{ $pengusaha->no_hp }}</td>
                        <td>{{ \Illuminate\Support\Str::limit($pengusaha->alamat, 10) }}</td>
                        <td>
                            <form onsubmit="return confirm('Apakah Anda Yakin ?');"
                                action="{{ route('usaha.destroy', $pengusaha->id) }}" method="POST">
                                <a href="{{ route('pengusaha.edit', $pengusaha->id) }}"
                                    class="btn btn-sm btn-outline-warning">EDIT</a>
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-sm btn-outline-danger">HAPUS</button>
                            </form>
                        </td>
                    </tr>

                @endforeach
            </tbody>
        </table>
    </div>

    @push('dataTableListUkm')
        <script>
            $(document).ready(function() {
                $('#usahatable').DataTable({
                    dom: 'lBfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
                });

            });
        </script>
    @endpush
@endsection
