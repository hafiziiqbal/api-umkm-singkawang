@extends('layouts.mainedit')

@section('container')
    <div style="margin-left:15%; margin-right:15%">
        @if ($errors->any())
            <div style="width:100%" class="alert alert-danger">
                <p><strong>Opps Ada Yang Harus Diperbaiki</strong></p>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <a href="{{ route('usaha.index') }}"><button class="btn btn-secondary">
                << Back</button></a>

        <h2 class="fw-bold mb-5 text-center">
            Memperbaru Data Pengusaha
        </h2>

        <h3 class="mb-4">
            A. Data Pemilik Usaha
        </h3>

        <form action="{{ route('pengusaha.update', $pengusahaById[0]->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')

            <input type="hidden" name="id" value="{{ $pengusahaById[0]->id }}">
            {{-- NIK --}}
            <div class="row align-items-start">
                <div class="p-2 mb-5 mt-3" style="width:35%;">
                    <label style="font-size: 17px">Nomor Induk Kependudukan (NIK)</label>
                </div>
                <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                    :
                </div>
                <div class="col">
                    <div class="mb-5 mt-3">
                        <input type="number" pattern="[0-9.]+" class="form-control p-2" aria-describedby="emailHelp"
                            name="nik" value="{{ $pengusahaById[0]->nik }}" required>
                    </div>
                </div>
            </div>

            {{-- Nama Pemilik Usaha --}}
            <div class="row align-items-start">
                <div class="p-2 mb-5 mt-3" style="width:35%;">
                    <label style="font-size: 17px">Nama Pemilik Usaha (sesuai KTP)</label>
                </div>
                <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                    :
                </div>
                <div class="col">
                    <div class="mb-5 mt-3">
                        <input type="text" class="form-control p-2" name="nama" required
                            value="{{ $pengusahaById[0]->nama }}">
                    </div>
                </div>
            </div>

            {{-- Jenis Kelamin --}}
            <div class="row align-items-start">
                <div class="p-2 mb-5 mt-3" style="width:35%;">
                    <label style="font-size: 17px">Jenis Kelamin</label>
                </div>
                <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                    :
                </div>
                <div class="col">
                    <div class="mb-5 mt-4 d-flex">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="jenis_kelamin" id="flexRadioDefault1"
                                value="Laki-Laki" {{ $pengusahaById[0]->jenis_kelamin == 'Laki-Laki' ? 'checked' : '' }}>
                            <label class="form-check-label" for="flexRadioDefault1">
                                Laki-Laki
                            </label>
                        </div>
                        &emsp;
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="jenis_kelamin" id="flexRadioDefault2"
                                value="Perempuan" {{ $pengusahaById[0]->jenis_kelamin == 'Perempuan' ? 'checked' : '' }}>
                            <label class="form-check-label" for="flexRadioDefault2">
                                Perempuan
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Alamat --}}
            <div class="row align-items-start">
                <div class="p-2 mb-5 mt-3" style="width:35%;">
                    <label style="font-size: 17px">Alamat Tempat Tinggal</label>
                </div>
                <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                    :
                </div>
                <div class="col">
                    <div class="mb-5 mt-3">
                        <textarea class="form-control" name="alamat" id="floatingTextarea2" style="height: 150px"
                            required>{{ $pengusahaById[0]->alamat }}</textarea>
                    </div>
                </div>
            </div>

            {{-- No Hp --}}
            <div class="row align-items-start">
                <div class="p-2 mb-5 mt-3" style="width:35%;">
                    <label style="font-size: 17px">No. Handphone</label>
                </div>
                <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                    :
                </div>
                <div class="col">
                    <div class="mb-5 mt-3">
                        <input type="number" pattern="[0-9.]+" class="form-control p-2" name="no_hp"
                            value="{{ $pengusahaById[0]->no_hp }}" required>
                    </div>
                </div>
            </div>

            <div class="row align-items-start">
                <div class="col- mb-4">
                    <button type="submit" class="btn btn-dark" style="width: 100%">Perbaharui Data Pengusaha</button>
                </div>
            </div>

        </form>
    </div>

@endsection
