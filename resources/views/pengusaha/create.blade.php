@extends('layouts.main')

@section('container')

    @if (session('message'))
        <div style="width: 70%" class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif
    @if ($errors->any())
        <div style="width:70%" class="alert alert-danger">
            <p><strong>Opps Ada Yang Harus Diperbaiki</strong></p>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <h2 class="fw-bold mb-5">
        Pendaftaran Pengusaha
    </h2>
    <form action="{{ route('pengusaha.store') }}" method="POST" enctype="multipart/form-data">
        @csrf

        {{-- NIK --}}
        <div class="row align-items-start" style="width:70%">
            <div class="p-2 mb-5 mt-3" style="width:35%;">
                <label style="font-size: 17px">Nomor Induk Kependudukan (NIK)</label>
            </div>
            <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                :
            </div>
            <div class="col">
                <div class="mb-5 mt-3">
                    <input type="number" pattern="[0-9.]+" class="form-control p-2" aria-describedby="emailHelp" name="nik"
                        required>
                </div>
            </div>
        </div>

        {{-- Nama Pemilik Usaha --}}
        <div class="row align-items-start" style="width:70%">
            <div class="p-2 mb-5 mt-3" style="width:35%;">
                <label style="font-size: 17px">Nama Pemilik Usaha (sesuai KTP)</label>
            </div>
            <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                :
            </div>
            <div class="col">
                <div class="mb-5 mt-3">
                    <input type="text" class="form-control p-2" name="nama" required>
                </div>
            </div>
        </div>

        {{-- Jenis Kelamin --}}
        <div class="row align-items-start" style="width:70%">
            <div class="p-2 mb-5 mt-3" style="width:35%;">
                <label style="font-size: 17px">Jenis Kelamin</label>
            </div>
            <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                :
            </div>
            <div class="col">
                <div class="mb-5 mt-4 d-flex">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="jenis_kelamin" id="flexRadioDefault1"
                            value="Laki-Laki">
                        <label class="form-check-label" for="flexRadioDefault1">
                            Laki-Laki
                        </label>
                    </div>
                    &emsp;
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="jenis_kelamin" id="flexRadioDefault2"
                            value="Perempuan" checked>
                        <label class="form-check-label" for="flexRadioDefault2">
                            Perempuan
                        </label>
                    </div>
                </div>
            </div>
        </div>

        {{-- Alamat --}}
        <div class="row align-items-start" style="width:70%">
            <div class="p-2 mb-5 mt-3" style="width:35%;">
                <label style="font-size: 17px">Alamat Tempat Tinggal</label>
            </div>
            <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                :
            </div>
            <div class="col">
                <div class="mb-5 mt-3">
                    <textarea class="form-control" name="alamat" id="floatingTextarea2" style="height: 150px"
                        required></textarea>
                </div>
            </div>
        </div>

        {{-- No Hp --}}
        <div class="row align-items-start" style="width:70%">
            <div class="p-2 mb-5 mt-3" style="width:35%;">
                <label style="font-size: 17px">No. Handphone</label>
            </div>
            <div class="col-1 p-2 mb-5 mt-3" style="width: auto">
                :
            </div>
            <div class="col">
                <div class="mb-5 mt-3">
                    <input type="number" pattern="[0-9.]+" class="form-control p-2" name="no_hp" required>
                </div>
            </div>
        </div>



        <div class="row align-items-start">
            <div class="col- mb-4">
                <button type="submit" class="btn btn-dark" style="width: 70%">SIMPAN UKM BARU</button>
            </div>
        </div>

    </form>




@endsection
