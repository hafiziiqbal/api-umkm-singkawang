<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsahasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usaha', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users')->cascadeOnDelete();;
            $table->string('nama');
            $table->bigInteger('nib')->nullable();
            $table->string('jenis');
            $table->foreignId('jenis_badan_usaha_id')->constrained('jenis_badan_usaha');
            $table->string('alamat');
            $table->bigInteger('aset');
            $table->bigInteger('rata_omset_perbulan');
            $table->integer('karyawan_lk');
            $table->integer('karyawan_pr');
            $table->text('kelurahan');
            $table->text('kecamatan');
            $table->string('sektor_usaha_id')->constrained('sektor_usaha');
            $table->string('bidang_usaha_id')->constrained('bidang_usaha');;
            $table->bigInteger('modal');
            $table->string('sektor');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('usaha', function (Blueprint $table) {
        //     $table->dropForeign('jenis_badan_usaha_id');
        // });
        Schema::dropIfExists('usaha');
    }
}
