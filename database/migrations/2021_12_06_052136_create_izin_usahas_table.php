<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIzinUsahasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('izin_usaha', function (Blueprint $table) {
            $table->id();
            $table->foreignId('izin');
            $table->date('tanggal');
            $table->bigInteger('nomor');
            $table->foreignId('usaha_id')->constrained('usaha')->cascadeOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('izin_usaha');
    }
}
