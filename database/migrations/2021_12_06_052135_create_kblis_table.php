<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKblisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kbli', function (Blueprint $table) {
            $table->id();
            $table->date('tanggal')->nullable();
            $table->string('iumk_nomor')->nullable();
            $table->foreignId('usaha_id')->constrained('usaha')->cascadeOnDelete();
            $table->foreignId('list_kbli_id')->nullable()->constrained('list_kbli');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kbli');
    }
}
