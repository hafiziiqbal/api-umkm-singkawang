<?php

namespace Database\Seeders;

use App\Models\SektorUsaha;
use Illuminate\Database\Seeder;

class SektorUsahaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SektorUsaha::create([
            'nama' => 'Industri Pengolahan',
        ]);
    }
}
