<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create([
            'name'  => 'change password',
            'guard_name' => 'api'
        ]);

        Permission::create([
            'name'  => 'kelola usaha',
            'guard_name' => 'web'
        ]);

        $rolePendata = Role::findByName('pendata');
        $rolePendata->givePermissionTo('kelola usaha');
    }
}
