<?php

namespace Database\Seeders;

use App\Models\JenisBadanUsaha;
use Illuminate\Database\Seeder;

class JenisBadanUsahaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        JenisBadanUsaha::create([
            'nama' => 'Perseorangan',
        ]);
        JenisBadanUsaha::create([
            'nama' => 'Firma',
        ]);
        JenisBadanUsaha::create([
            'nama' => 'CV',
        ]);
        JenisBadanUsaha::create([
            'nama' => 'Persekutuan Perdata',
        ]);
    }
}
