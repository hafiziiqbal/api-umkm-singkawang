<?php

namespace Database\Seeders;

use App\Models\ListKbli;
use Illuminate\Database\Seeder;

class ListKbliSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ListKbli::create([
            'no_kbli' => '66292',
            'nama' => 'AKTIVITAS PEMERINGKAT USAHA MIKRO, KECIL, MENENGAH DAN KOPERASI',
        ]);

        ListKbli::create([
            'no_kbli' => '47111',
            'nama' => 'PERDAGANGAN ECERAN BERBAGAI MACAM BARANG YANG UTAMANYA MAKANAN, MINUMAN ATAU TEMBAKAU DI MINIMARKET/SUPERMARKET/HYPERMARKET ',
        ]);

        ListKbli::create([
            'no_kbli' => '47112',
            'nama' => 'PERDAGANGAN ECERAN BERBAGAI MACAM BARANG YANG UTAMANYA MAKANAN, MINUMAN ATAU TEMBAKAU BUKAN DI MINIMARKET/SUPERMARKET/HYPERMARKET (TRADISIONAL) ',
        ]);
    }
}
