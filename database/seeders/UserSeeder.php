<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pendata = User::create([
            'nik' => '3576014403910003',
            'nama' => 'Hafizi Iqbal',
            'jenis_kelamin' => 'Laki-Laki',
            'no_hp' => '089513591811',
            'alamat' => 'Kota Pontianak',
            'password' => bcrypt('hafiziiqbal'),
        ]);

        $pendata->assignRole('pendata');

        $pendata1 = User::create([
            'nik' => '3576014403910043',
            'nama' => 'Sri Lisanti',
            'jenis_kelamin' => 'Perempuan',
            'no_hp' => '0895365117411',
            'alamat' => 'Kota Pontianak',
            'password' => bcrypt('srilisanti123'),
        ]);

        $pendata1->assignRole('pendata');

        $pendata2 = User::create([
            'nik' => '3576014403911111',
            'nama' => 'Veny Jasmayanti',
            'jenis_kelamin' => 'Perempuan',
            'no_hp' => '081258260241',
            'alamat' => 'Kota Pontianak',
            'password' => bcrypt('veny123'),
        ]);

        $pendata2->assignRole('pendata');
    }
}
