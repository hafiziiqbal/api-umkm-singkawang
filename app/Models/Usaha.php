<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Usaha extends Model
{
    use HasFactory;

    protected $table = 'usaha';

    protected $fillable = [
        'user_id',
        'nama',
        'nib',
        'jenis',
        'perizinan_id',
        'jenis_badan_usaha_id',
        'aset',
        'rata_omset_perbulan',
        'karyawan_lk',
        'karyawan_pr',
        'kelurahan',
        'kecamatan',
        'sektor_usaha',
        'bidang_usaha',
        'modal',
        'sektor'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function perizinan()
    {
        return $this->hasMany(Perizinan::class, 'usaha_id');
    }

    public function jenisBadanUsaha()
    {
        return $this->belongsTo(JenisBadanUsaha::class, 'jenis_badan_usaha_id');
    }

    public function gambar()
    {
        return $this->hasMany(Foto::class, 'usaha_id');
    }
}
