<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ListKbli extends Model
{
    use HasFactory;
    protected $table = 'list_kbli';
    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
