<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IzinUsaha extends Model
{
    use HasFactory;
    protected $table = 'izin_usaha';
    protected $fillable = [
        'izin',
        'nomor',
        'tanggal',
        'usaha_id',
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
