<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Foto extends Model
{
    use HasFactory;
    protected $table = 'foto';
    protected $fillable = [
        'foto',
        'jenis',
        'nama',
        'usaha_id',
    ];
    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
