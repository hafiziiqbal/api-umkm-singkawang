<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kbli extends Model
{
    use HasFactory;
    protected $table = 'kbli';
    protected $fillable = [
        'tanggal',
        'iumk_nomor',
        'list_kbli_id',
        'usaha_id',
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function listKbli()
    {
        return $this->belongsTo(Kbli::class, 'kbli_id');
    }
}
