<?php

namespace App\Http\Requests\Usaha;

use Illuminate\Foundation\Http\FormRequest;

class StoreUsahaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nik'                               => 'required|numeric|digits:16|unique:users,nik',
            'no_hp'                             => 'required|numeric|digits_between:4,14|regex:/(08)[0-9]{4,14}$/|unique:users',
            'nama'                              => 'required|regex:/^([a-zA-Z]+)(\s[a-zA-Z]+)*$/|max:60',
            'jenis_kelamin'                     => 'required|in:Laki-Laki,Perempuan',
            'alamat'                            => 'required|max:255',

            'nama_usaha'                        => 'required|max:255',
            'nib'                               => 'required|numeric|digits:13',
            'jenis_usaha'                       => 'required|max:255',
            'alamat_usaha'                      => 'required|max:255',
            'aset'                              => 'required|numeric',
            'rata_omset_perbulan'               => 'required|numeric',
            'karyawan_lk'                       => 'required|numeric',
            'karyawan_pr'                       => 'required|numeric',
            'jenis_badan_usaha'                 => 'required|numeric',
            'perizinan.*.tanggal'               => 'required|date',
            'perizinan.*.iumk'                  => 'required|max:255',
            'perizinan.*.kbli_id'               => 'required|numeric|exists:kbli,id',
            'logo'                              => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'produk.*'                          => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'lokasi.*'                          => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ];
    }

    public function messages()
    {
        return [
            'nik.required'                      => 'NIK harus diisi',
            'nik.numeric'                       => 'NIK harus berupa angka',
            'nik.digits'                        => 'NIK harus 16 digit',
            'nik.unique'                        => 'NIK sudah dipakai',

            'no_hp.required'                    => 'No. HP harus diisi',
            'no_hp.numeric'                     => 'No. HP harus berupa angka',
            'no_hp.digits_between'              => 'No. HP minimal 4 digit dan maksimal 14 digit',
            'no_hp.unique'                      => 'No. HP Sudah Dipakai',
            'no_hp.regex'                       => 'No. HP harus diawali dengan 08',

            'nama.required'                     => 'Nama harus diisi',
            'nama.regex'                        => 'Nama harus berupa karakter A-Z',
            'nama.max'                          => 'Nama harus maksimal 60 karakter',

            'jenis_kelamin.required'            => "Jenis Kelamin harus diisi",
            'jenis_kelamin.in'                  => "Jenis Kelamin harus Laki-Laki atau Perempuan",

            'alamat.required'                   => 'Alamat harus diisi',
            'alamat.max'                        => 'Alamat harus maksimal 255 karakter',

            'nama_usaha.required'               => 'Nama Usaha harus diisi',
            'nama_usaha.max'                    => 'Nama Usaha harus maksimal 255 karaker',

            'nib.required'                      => 'NIB harus diisi',
            'nib.numeric'                       => 'NIB harus berupa angka',
            'nib.digits'                        => 'NIB harus 13 digit',

            'jenis_usaha.required'              => 'Jenis Usaha harus diisi',
            'jenis_usaha.max'                   => 'Jenis Usaha harus maksimal 255 karakter',

            'alamat_usaha.required'             => 'Alamat Usaha harus diisi',
            'alamat_usaha.max'                  => 'Alamat Usaha maksimal harus 255 karakter',

            'asset.required'                    => 'Aset harus diisi',
            'asset.numeric'                     => 'Aset harus berupa angka',

            'rata_omset_perbulan.required'      => 'Rata Omset Perbulan harus diisi',
            'rata_omset_perbulan.numeric'       => 'Rata Omset Perbulan harus berupa angka',

            'karyawan_lk.required'              => 'Jumlah Karyawan Laki-Laki tidak boleh kosong',
            'karyawan_lk.numeric'               => 'Jumlah Karyawan Laki-Laki harus berupa angka',

            'karyawan_pr.required'              => 'Jumlah Karyawan Laki-Laki tidak boleh kosong',
            'karyawan_pr.numeric'               => 'Jumlah Karyawan Laki-Laki harus berupa angka',

            'jenis_badan_usaha.required'        => 'Jenis Badan Usaha harus diisi',
            'jenis_badan_usaha.numeric'         => 'Jenis Badan Usaha harus berupa angka',

            'perizinan.*.tanggal.required'      => 'Tanggal Perizinan harus diisi',
            'perizinan.*.tanggal.date'          => 'Tanggal Perizinan harus format tahun/bulan/tanggal',

            'perizinan.*.iumk.max'              => 'No. IUMK harus maksimal 255 karakter',
            'perizinan.*.iumk.required'         => 'No. IUMK harus diisi',

            'perizinan.*.kbli_id.numeric'       => 'KBLI harus berupa angka',
            'perizinan.*.kbli_id.required'      => 'KBLI harus diisi',
            'perizinan.*.kbli_id.exists'        => 'KBLI tidak valid',

            'logo.required'                     => 'Logo harus diisi',
            'logo.image'                        => 'Logo harus berupa gambar',
            'logo.mimes'                        => 'Logo harus format JPEG, PNG, JPG',
            'logo.max'                          => 'Logo maksimal 2MB',

            'produk.*.required'                 => 'Gambar Produk harus diisi',
            'produk.*.image'                    => 'Gambar Produk harus berupa gambar',
            'produk.*.mimes'                    => 'Gambar Produk format JPEG, PNG, JPG',
            'produk.*.max'                      => 'Gambar Produk maksimal 2MB',


            'lokasi.*'                          => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'lokasi.*.required'                 => 'Gambar Produk harus diisi',
            'lokasi.*.image'                    => 'Gambar Produk harus berupa gambar',
            'lokasi.*.mimes'                    => 'Gambar Produk format JPEG, PNG, JPG',
            'lokasi.*.max'                      => 'Gambar Produk maksimal 2MB',
        ];
    }
}
