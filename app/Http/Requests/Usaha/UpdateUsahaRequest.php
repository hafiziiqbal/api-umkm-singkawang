<?php

namespace App\Http\Requests\Usaha;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUsahaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama_usaha'                        => 'max:255|nullable',
            'nib'                               => 'numeric|digits:13|nullable',
            'jenis_usaha'                       => 'max:255|nullable',
            'alamat_usaha'                      => 'max:255|nullable',
            'aset'                              => 'numeric|nullable',
            'rata_omset_perbulan'               => 'numeric|nullable',
            'karyawan_lk'                       => 'numeric|nullable',
            'karyawan_pr'                       => 'numeric|nullable',
            'jenis_badan_usaha'                 => 'numeric|nullable',
            'perizinan.*.tanggal'               => 'date',
            'perizinan.*.iumk_nomor'            => 'max:255',
            'perizinan.*.kbli_id'               => 'numeric|exists:kbli,id',
            'logo'                              => 'image|mimes:jpeg,png,jpg|max:2048',
            'produk.*'                          => 'image|mimes:jpeg,png,jpg|max:2048',
            'lokasi.*'                          => 'image|mimes:jpeg,png,jpg|max:2048',
        ];
    }

    public function messages()
    {
        return [
            'nama_usaha.max'                    => 'Nama Usaha harus maksimal 255 karaker',

            'nib.numeric'                       => 'NIB harus berupa angka',
            'nib.digits'                        => 'NIB harus 13 digit',

            'jenis_usaha.max'                   => 'Jenis Usaha harus maksimal 255 karakter',

            'alamat_usaha.max'                  => 'Alamat Usaha maksimal harus 255 karakter',

            'asset.numeric'                     => 'Aset harus berupa angka',

            'rata_omset_perbulan.numeric'       => 'Rata Omset Perbulan harus berupa angka',

            'karyawan_lk.numeric'               => 'Jumlah Karyawan Laki-Laki harus berupa angka',

            'karyawan_pr.numeric'               => 'Jumlah Karyawan Laki-Laki harus berupa angka',

            'jenis_badan_usaha.numeric'         => 'Jenis Badan Usaha harus berupa angka',

            'perizinan.*.tanggal.required'      => 'Tanggal Perizinan harus diisi',
            'perizinan.*.tanggal.date'          => 'Tanggal Perizinan harus format tahun/bulan/tanggal',

            'perizinan.*.iumk.max'              => 'No. IUMK harus maksimal 255 karakter',
            'perizinan.*.iumk.required'         => 'No. IUMK harus diisi',

            'perizinan.*.kbli_id.numeric'       => 'KBLI harus berupa angka',
            'perizinan.*.kbli_id.required'      => 'KBLI harus diisi',
            'perizinan.*.kbli_id.exists'        => 'KBLI tidak valid',

            'logo.image'                        => 'Logo harus berupa gambar',
            'logo.mimes'                        => 'Logo harus format JPEG, PNG, JPG',
            'logo.max'                          => 'Logo maksimal 2MB',

            'produk.*.image'                    => 'Gambar Produk harus berupa gambar',
            'produk.*.mimes'                    => 'Gambar Produk format JPEG, PNG, JPG',
            'produk.*.max'                      => 'Gambar Produk maksimal 2MB',

            'lokasi.*.image'                    => 'Gambar Produk harus berupa gambar',
            'lokasi.*.mimes'                    => 'Gambar Produk format JPEG, PNG, JPG',
            'lokasi.*.max'                      => 'Gambar Produk maksimal 2MB',
        ];
    }
}
