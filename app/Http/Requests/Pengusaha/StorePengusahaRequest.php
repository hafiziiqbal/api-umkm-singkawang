<?php

namespace App\Http\Requests\Pengusaha;

use Illuminate\Foundation\Http\FormRequest;

class StorePengusahaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nik'                               => 'required|numeric|digits:16|unique:users,nik',
            'no_hp'                             => 'required|numeric|digits_between:4,14|unique:users|regex:/(08)[0-9]{4,14}$/',
            'nama'                              => 'required|regex:/^([a-zA-Z]+)(\s[a-zA-Z]+)*$/|max:60',
            'jenis_kelamin'                     => 'required|in:Laki-Laki,Perempuan',
            'alamat'                            => 'required|max:255',
        ];
    }

    public function messages()
    {
        return [
            'nik.required'                      => 'NIK harus diisi',
            'nik.numeric'                       => 'NIK harus berupa angka',
            'nik.digits'                        => 'NIK harus 16 digit',
            'nik.unique'                        => 'NIK sudah dipakai',

            'no_hp.required'                    => 'No. HP harus diisi',
            'no_hp.numeric'                     => 'No. HP harus berupa angka',
            'no_hp.digits_between'              => 'No. HP minimal 4 digit dan maksimal 14 digit',
            'no_hp.unique'                      => 'No. HP Sudah Dipakai',
            'no_hp.regex'                       => 'No. HP harus diawali dengan 08',

            'nama.required'                     => 'Nama harus diisi',
            'nama.regex'                        => 'Nama harus berupa karakter A-Z',
            'nama.max'                          => 'Nama harus maksimal 60 karakter',

            'jenis_kelamin.required'            => "Jenis Kelamin harus diisi",
            'jenis_kelamin.in'                  => "Jenis Kelamin harus Laki-Laki atau Perempuan",

            'alamat.required'                   => 'Alamat harus diisi',
            'alamat.max'                        => 'Alamat harus maksimal 255 karakter',
        ];
    }
}
