<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\VerificationOtp;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class PasswordController extends Controller
{
    public function forgotPassword(Request $request)
    {
        $daftarUser = User::where('no_hp', '=', $request->no_hp)->first();
        $daftarVerificationOtp = VerificationOtp::where('user_id', '=', $daftarUser->id)->first();

        //Jika Nomor Telp Tidak Ada
        if ($daftarUser === null) {
            return response()->json([
                'message' => 'Nomor Telepon Anda Tidak Ada',
            ], 400);
        }

        //Jika verifikasiOTP sudah ada maka tentukan rentang waktu
        if ($daftarVerificationOtp != null) {
            $timeNow = Carbon::now();
            $countTime = date_diff($timeNow, $daftarVerificationOtp->updated_at);
        }

        //Jika OTP belum ada atau masa tunggu sudah 24 jam
        if ($daftarVerificationOtp === null || $countTime->h >= 24) {
            if ($daftarVerificationOtp === null) {
                //Mengisi Kode OTP
                $verificationOtpObject = new VerificationOtp([
                    'otp' => rand(100000, 999999),
                    'status' => 1
                ]);

                //Menyimpan Kode OTP ke tabel
                $daftarUser->verificationOtp()->save($verificationOtpObject);
            } else {
                $daftarUser->verificationOtp()->update([
                    'otp' => rand(100000, 999999),
                    'status' => 1
                ]);
                $verificationOtpObject = VerificationOtp::where('user_id', '=', $daftarUser->id)->first();
            }

            //Mengirim OTP Ke User 
            if (!preg_match('/[^+0-9]/', trim($daftarUser->no_hp))) {
                // cek apakah no hp karakter 1-3 adalah +62
                if (substr(trim($daftarUser->no_hp), 0, 3) == '+62') {
                    $hp = trim($daftarUser->no_hp);
                }
                // cek apakah no hp karakter 1 adalah 0
                elseif (substr(trim($daftarUser->no_hp), 0, 1) == '0') {
                    $hp = '+62' . substr(trim($daftarUser->no_hp), 1);
                }
            }
            $to = $hp;
            $from = getenv("TWILIO_FROM");
            $message = 'Kode OTP Anda Adalah ' . $verificationOtpObject->otp;


            $client = new Client(['auth' => [getenv("TWILIO_SID"), getenv("TWILIO_TOKEN")]]);
            $response =  $client->post(
                'https://api.twilio.com/2010-04-01/Accounts/' . getenv("TWILIO_SID") . '/Messages.json',
                ['form_params' => [
                    'Body' => $message, //set message body
                    'To' => $to,
                    'From' => $from //we get this number from twilio
                ]]
            );

            if ($response->getStatusCode() === 201) {
                return response()->json([
                    'message' => 'Kode OTP Berhasil Dibuat Silahkan Cek Ponsel Anda'

                ], 201);
            } else {
                $response->getBody()->getContents();
            }
        }

        //Jika OTP Sudah mencapai 15 menit ubah stata jadi 0
        if ($countTime->i >= 15) {
            $daftarVerificationOtp->update(['status' => 0]);
            return response()->json([
                'message' => 'Masa aktif OTP anda sudah habis, coba lagi dalam ' . (24 - $countTime->h) . ' jam kedepan',
            ], 400);
        }

        //Jika OTP Sudah ada dan masih aktif
        if ($daftarVerificationOtp->otp != null && $daftarVerificationOtp->status === 1) {
            return response()->json([
                'message' => 'Kode OTP anda sudah dikirimkan dan masa aktif OTP anda tersisa ' . (15 - $countTime->i) . ' menit',
            ]);
        }

        //Jika OTP Sudah ada dan tidak aktif
        if ($daftarVerificationOtp->otp != null && $daftarVerificationOtp->status === 0) {
            return response()->json([
                'message' => 'Masa aktif OTP anda sudah habis, coba lagi dalam ' . (24 - $countTime->h) . ' jam kedepan',
            ]);
        }
    }

    public function verificationOtp(Request $request)
    {

        $daftarKodeOtp = VerificationOtp::where('otp', '=', $request->otp)->first();



        if ($daftarKodeOtp != null) {
            $timeNow = Carbon::now();
            $countTime = date_diff($timeNow, $daftarKodeOtp->updated_at);
        }

        //Jika OTP yang dimasukkan tidak cocok
        if ($daftarKodeOtp === null) {
            return response()->json([
                'message' => 'Kode OTP ANDA SALAH',
            ], 400);
        }

        $verificationOtpWithUser = $daftarKodeOtp->with('user')->get();
        foreach ($verificationOtpWithUser as  $getUser) {
            $user = $getUser->user;
        }


        //Jika OTP cocok tapi sudah tidak aktif
        if ($daftarKodeOtp != null && $countTime->h >= 24) {
            $daftarKodeOtp->update(['status' => 0]);
            //menghapus permission ubah password kepada user
            $user->revokePermissionTo('change password');
        }

        //Jika status 0
        if ($daftarKodeOtp->status === 0) {
            return response()->json([
                'message' => 'Masa aktif OTP anda sudah habis, coba lagi dalam ' . (24 - $countTime->h) . ' jam kedepan',
            ], 400);
        }

        //memberikan permission kepada user untuk mengubah password
        $user->givePermissionTo('change password');


        return response()->json([
            'message' => 'Verifikasi OTP Berhasil',
        ], 200);
    }

    public function changePassword(Request $request)
    {
        $daftarUser = User::where('no_hp', '=', $request->no_hp)->first();

        if (!$daftarUser->hasPermissionTo('change password')) {
            return response()->json([
                'message' => 'Anda Belum Memasukan Kode OTP',
            ], 403);
        }


        $daftarUser->update(
            [
                'password' => bcrypt($request->newPassword)
            ]
        );

        //menghapus permission ubah password kepada user
        $daftarUser->revokePermissionTo('change password');

        return response()->json([
            'message' => 'Password berhasil diupdate',
        ], 200);
    }
}
