<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\JenisBadanUsaha;

class JenisBadanUsahaController extends Controller
{
    public function index()
    {
        $daftarJenisBadanUsaha = JenisBadanUsaha::all();
        return response()->json($daftarJenisBadanUsaha, 200);
    }
}
