<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Kbli;

class KbliController extends Controller
{
    public function index()
    {
        //daftarKbli
        $daftarKbli = Kbli::all();
        return response()->json($daftarKbli, 200);
    }
}
