<?php

namespace App\Http\Controllers\API;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequestAuth;
use App\Http\Requests\LogoutRequestAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends Controller
{
    public function login(LoginRequestAuth $request)
    {
        $credentials = $request->only('no_hp', 'password');
        try {
            //Jika credentialas yang dimasukan tidak sesuai dengan akun yang ada
            if (!$token = JWTAuth::attempt($credentials, ['exp' => Carbon::now()->addDays(7)->timestamp])) {
                return response()->json([
                    'success' => false,
                    'message' => 'Login credentials are invalid.',
                ], 400);
            }

            $user = User::where('no_hp', $request->no_hp)->first();
            //Mengembalikan nilai token dan user
            return response()->json([
                'success' => true,
                'token' => $token,
                'user'  => $user
            ], 200);
        } catch (JWTException $e) {
            return response()->json([
                'success' => false,
                'message' => 'Could not create token ' . $e,
            ], 500);
        }
    }

    public function logout(LogoutRequestAuth $request)
    {

        try {
            JWTAuth::invalidate($request->token);

            return response()->json([
                'success' => true,
                'message' => 'User has been logged out'
            ], 200);
        } catch (JWTException $e) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, user cannot be logged out ' . $e
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
