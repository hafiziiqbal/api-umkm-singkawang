<?php

namespace App\Http\Controllers\API;

use App\Models\Foto;
use App\Models\User;
use App\Models\Usaha;
use App\Models\Perizinan;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRequestUsaha;
use App\Http\Requests\UpdateRequestUsaha;
use Illuminate\Http\Request;

class UsahaController extends Controller
{

    public function index()
    {
        $relasiUsaha = Usaha::with('user', 'jenisBadanUsaha', 'gambar', 'perizinan', 'perizinan.kbli')->get();
        return response()->json($relasiUsaha, 200);
    }

    public function usahaById($id)
    {
        //Mencari User Yang Login Berdasarkan Token
        $user = JWTAuth::User();
        foreach ($user->roles as $role) {
            $roleName = $role->name;
        }

        //Mencari usaha Berdasarkan Id usaha
        $usahaId = Usaha::where('id', $id)->get();
        foreach ($usahaId as $usaha) {
            $userId = $usaha->user_id;
        }

        //Jika Rolenya Pengusaha dan id usernya tidak sama dengan id user yang ada di usaha 
        if ($roleName === 'pengusaha' && $user->id != $userId) {
            return response()->json([
                'message' => 'Anda hanya bisa melihat usaha anda sendiri'
            ], 403);
        }
        $usahaById = Usaha::where('id', $id)->with('user', 'jenisBadanUsaha', 'gambar', 'perizinan', 'perizinan.kbli')->get();
        return response()->json($usahaById, 200);
    }

    public function store(StoreRequestUsaha $request)
    {
        //input user
        $user = new User;
        $user->nik            = $request->nik;
        $user->nama           = $request->nama;
        $user->jenis_kelamin  = $request->jenis_kelamin;
        $user->no_hp          = $request->no_hp;
        $user->alamat         = $request->alamat;
        $user->password       = bcrypt(strtolower(preg_replace("/[^a-zA-Z]/", "", $request->nama)) . "123");
        $user->save();

        //menetapkan role pengusaha
        $userById = User::find($user->id);
        $userById->assignRole('pengusaha');

        //input data usaha
        $usaha = new Usaha(
            [
                'nama'                  => $request->data_usaha['nama_usaha'],
                'nib'                   => $request->data_usaha['nib'],
                'jenis'                 => $request->data_usaha['jenis_usaha'],
                'jenis_badan_usaha_id'  => $request->data_usaha['jenis_badan_usaha'],
                'alamat'                => $request->data_usaha['alamat_usaha'],
                'aset'                  => $request->data_usaha['aset'],
                'rata_omset_perbulan'   => $request->data_usaha['rata_omset_perbulan'],
                'karyawan_lk'           => $request->data_usaha['karyawan_lk'],
                'karyawan_pr'           => $request->data_usaha['karyawan_pr'],
            ]
        );

        $saveUsaha =  $userById->usaha()->save($usaha);

        //input data izin
        foreach ($request->perizinan as $izin) {
            $perizinan = new Perizinan(
                [
                    'tanggal'       => $izin['tanggal'],
                    'iumk_nomor'    => $izin['iumk_nomor'],
                    'kbli_id'       => $izin['kbli_id']
                ]
            );
            $usahaById = Usaha::find($saveUsaha->id);
            $usahaById->perizinan()->save($perizinan);
        }



        // input gambar logo
        $imgLogoName = 'logo_' . $request->file('logo')->hashName();
        $request->file('logo')->storeAs('public/uploads/img', $imgLogoName);
        $gambarLogo = new Foto(
            [
                'foto'          => 'storage/uploads/img/' . $imgLogoName,
                'jenis'    => "logo",
                'nama'     => "Logo " . $saveUsaha->nama_usaha
            ]
        );
        $usahaById = Usaha::find($saveUsaha->id);
        $usahaById->gambar()->save($gambarLogo);

        //input gambar produk
        foreach ($request->file('produk') as $key => $imgProduk) {
            $imgProdukName      = 'produk_' . $imgProduk->hashName();
            $imgProduk->storeAs('public/uploads/img', $imgProdukName);
            $gambarProduk  = new Foto(
                [
                    'foto'          => 'storage/uploads/img/' . $imgProdukName,
                    'jenis'    => "produk",
                    'nama'     => "Produk " . $key + 1
                ]
            );
            $usahaById = Usaha::find($saveUsaha->id);
            $usahaById->gambar()->save($gambarProduk);
        }

        //input gambar lokasi
        foreach ($request->file('lokasi') as $key => $imgLokasi) {
            $imgLokasiName      = 'lokasi_' . $imgLokasi->hashName();
            $imgLokasi->storeAs('public/uploads/img', $imgLokasiName);
            $gambarLokasi  = new Foto(
                [
                    'foto'          => 'storage/uploads/img/' . $imgLokasiName,
                    'jenis'    => "lokasi",
                    'nama'     => "Lokasi " . $key + 1
                ]
            );
            $usahaById = Usaha::find($saveUsaha->id);
            $usahaById->gambar()->save($gambarLokasi);
        }

        $relasiUsahaById = Usaha::where('id', $saveUsaha->id)->with('user', 'jenisBadanUsaha', 'gambar', 'perizinan', 'perizinan.kbli')->get();
        return response()->json([
            'message'   => 'Success',
            'data'      => $relasiUsahaById
        ], 200);
    }

    public function destroy($id)
    {
        $usaha = Usaha::find($id);
        $user = User::find($usaha->user_id);

        if (is_null($usaha)) {
            return response()->json("Data Not Found", 404);
        }

        $fotoByUsahaId = Foto::where('usaha_id', $id)->get();
        foreach ($fotoByUsahaId as $foto) {
            $imagePath = public_path() . '/' . $foto->foto;
            unlink($imagePath);
        }

        $hapusSuccess = $user->delete();

        if (!$hapusSuccess) {
            return response()->json("Error Delete", 500);
        } else {
            return response()->json("$usaha->nama with id $id has been deleted", 200);
        }
    }

    public function update(UpdateRequestUsaha $request, $id)
    {

        //Mencari Role Name Sesuai User yang login
        $userLogin = JWTAuth::User();
        foreach ($userLogin->roles as $role) {
            $role_name = $role->name;
        }

        if ($role_name === 'pengusaha' && $id != $userLogin->id) {
            return response()->json([
                'message' => 'Anda hanya bisa mengupdate usaha anda sendiri'
            ], 403);
        }

        //Mencari Usaha Sesuai Id Usaha
        $usahaById = Usaha::find($id);
        $userById = User::find($usahaById->user_id);

        // Update User
        $password = bcrypt(strtolower(preg_replace("/[^a-zA-Z]/", "", $request->nama)) . "123");
        $arrayUser = array(

            'nik'           => $request->nik,
            'nama'          => $request->nama,
            'jenis_kelamin' => $request->jenis_kelamin,
            'no_hp'         => $request->no_hp,
            'alamat'        => $request->alamat,
            'password'      => $password

        );
        $userById->update($arrayUser);

        //Update data usaha
        $arrayUsaha = array(
            'nama'            => $request->data_usaha['nama_usaha'],
            'nib'                   => $request->data_usaha['nib'],
            'jenis'           => $request->data_usaha['jenis_usaha'],
            'jenis_badan_usaha_id'  => $request->data_usaha['jenis_badan_usaha'],
            'alamat'          => $request->data_usaha['alamat_usaha'],
            'aset'                  => $request->data_usaha['aset'],
            'rata_omset_perbulan'   => $request->data_usaha['rata_omset_perbulan'],
            'karyawan_lk'           => $request->data_usaha['karyawan_lk'],
            'karyawan_pr'           => $request->data_usaha['karyawan_pr'],
        );
        $userById->usaha()->update($arrayUsaha);


        if ($request->has('perizinan')) {
            //Hapus Perizinan
            $perizinanByUsahaId = Perizinan::where('usaha_id', $id)->get();
            foreach ($perizinanByUsahaId as $perizinanByUsahaId) {
                $perizinanByUsahaId->delete();
            }
            //Update data izin
            foreach ($request->perizinan as $izin) {
                $izin = new Perizinan(
                    [
                        'tanggal'       => $izin['tanggal'],
                        'iumk_nomor'    => $izin['iumk_nomor'],
                        'kbli_id'       => $izin['kbli_id']
                    ]
                );
                $usahaById->perizinan()->save($izin);
            }
        }


        if ($request->has('logo')) {
            //Hapus Gambar
            $gambarByUsahaId = Foto::where(['usaha_id' => $id, 'jenis' => 'logo'])->get();

            if ($gambarByUsahaId->all() != null) {
                $imagePath = public_path() . '/' . $gambarByUsahaId[0]->foto;
                unlink($imagePath);
                $gambarByUsahaId[0]->delete();
            }


            // Update gambar logo
            $imgLogoName = 'logo_' . $request->file('logo')->hashName();
            $request->file('logo')->storeAs('public/uploads/img', $imgLogoName);
            $gambarLogo = new Foto(
                [
                    'foto'          => 'storage/uploads/img/' . $imgLogoName,
                    'jenis'         => "logo",
                    'nama'          => "Logo " . $usahaById->nama_usaha
                ]
            );
            $usahaById->gambar()->save($gambarLogo);
        }

        //hapus gambar produk
        if ($request->has('imgproduk_id')) {
            foreach ($request->imgproduk_id as $key => $produkId) {
                $gambarById = Foto::where(['id' => $produkId, 'jenis' => 'produk'])->get();
                $imagePath = public_path() . '/' . $gambarById[0]->foto;
                unlink($imagePath);
                $gambarById[0]->delete();
            }
        }

        if ($request->has('produk')) {

            //Update gambar produk
            foreach ($request->file('produk') as $key => $imgProduk) {
                $imgProdukName      = 'produk_' . $imgProduk->hashName();
                $imgProduk->storeAs('public/uploads/img', $imgProdukName);
                $gambarProduk  = new Foto(
                    [
                        'foto'          => 'storage/uploads/img/' . $imgProdukName,
                        'jenis'         => "produk",
                        'nama'          => "Produk " . $key + 1
                    ]
                );
                $usahaById->gambar()->save($gambarProduk);
            }
        }

        //hapus gambar lokasi
        if ($request->has('imglokasi_id')) {
            foreach ($request->imglokasi_id as $key => $lokasiId) {
                $gambarById = Foto::where(['id' => $lokasiId, 'jenis' => 'lokasi'])->get();
                $imagePath = public_path() . '/' . $gambarById[0]->foto;
                unlink($imagePath);
                $gambarById[0]->delete();
            }
        }

        if ($request->has('lokasi')) {
            //Update gambar lokasi
            foreach ($request->file('lokasi') as $key => $imgLokasi) {
                $imgLokasiName      = 'lokasi_' . $imgLokasi->hashName();
                $imgLokasi->storeAs('public/uploads/img', $imgLokasiName);
                $gambarLokasi  = new Foto(
                    [
                        'foto'          => 'storage/uploads/img/' . $imgLokasiName,
                        'jenis'         => "lokasi",
                        'nama'          => "Lokasi " . $key + 1
                    ]
                );
                $usahaById->gambar()->save($gambarLokasi);
            }
        }

        $relasiUsahaById = Usaha::where('id', $id)->with('user', 'jenisBadanUsaha', 'gambar', 'perizinan', 'perizinan.kbli')->get();
        return response()->json([
            'message'   => 'Success',
            'data'      => $relasiUsahaById
        ], 200);
    }
}
