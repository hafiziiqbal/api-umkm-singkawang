<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\ClientException;
use Nexmo\Laravel\Facede\Nexmo;

class SmsController extends Controller
{

    public function sendSms(Request $request)
    {

        foreach ($request->user_id as  $penerimaId) {
            $daftarUsers = User::where('id', $penerimaId)->get();
            foreach ($daftarUsers as  $user) {
                if (!preg_match('/[^+0-9]/', trim($user->no_hp))) {
                    // cek apakah no hp karakter 1-3 adalah +62
                    if (substr(trim($user->no_hp), 0, 3) == '+62') {
                        $hp = trim($user->no_hp);
                    }
                    // cek apakah no hp karakter 1 adalah 0
                    elseif (substr(trim($user->no_hp), 0, 1) == '0') {
                        $hp = '+62' . substr(trim($user->no_hp), 1);
                    }
                }
            }

            $to = $hp;
            $from = getenv("TWILIO_FROM");
            $message = $request->message;

            try {
                $client = new Client(['auth' => [getenv("TWILIO_SID"), getenv("TWILIO_TOKEN")]]);
                $client->post(
                    'https://api.twilio.com/2010-04-01/Accounts/' . getenv("TWILIO_SID") . '/Messages.json',
                    ['form_params' => [
                        'Body' => $message, //set message body
                        'To' => $to,
                        'From' => $from //we get this number from twilio
                    ]]
                );
            } catch (ClientException $e) {
                // $messageError[] = 'Pesan dengan nomor ' . $hp . ' gagal dikirim ';
                $responseContents = $e->getResponse()->getBody();
                $responseToJson =  json_decode($responseContents);
                return $responseToJson;
            }
        }
        return response()->json([
            'message' => 'Kirim Pesan Berhasil'
        ]);
    }
}
