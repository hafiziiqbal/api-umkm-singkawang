<?php

namespace App\Http\Controllers\Web;

use App\Models\User;
use App\Models\Usaha;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Pengusaha\StorePengusahaRequest;
use App\Http\Requests\Pengusaha\UpdatePengusahaRequest;
use App\Http\Requests\StoreRequestPengusaha;

class PengusahaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $daftarPengusaha = User::role('pengusaha')->get();
        return view('pengusaha.listpengusaha')->with(['tittle' => 'LIST PENGUSAHA', 'daftarPengusaha' => $daftarPengusaha]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('pengusaha.create', ['tittle' => 'PENDAFTARAN PENGUSAHA']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePengusahaRequest $request)
    {
        //input user
        $user = new User;
        $user->nik            = $request->nik;
        $user->nama           = $request->nama;
        $user->jenis_kelamin  = $request->jenis_kelamin;
        $user->no_hp          = $request->no_hp;
        $user->alamat         = $request->alamat;
        $user->password       = bcrypt(strtolower(preg_replace("/[^a-zA-Z]/", "", $request->nama)) . "123");
        $user->save();

        //menetapkan role pengusaha
        $userById = User::find($user->id);
        $userById->assignRole('pengusaha');

        return redirect()->route('pengusaha.create')->with(['message' => 'Pengusaha Baru Berhasil Diinput']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pengusahaById = User::where('id', $id)->get();

        return view('pengusaha.edit')->with(['tittle' => 'EDIT', 'pengusahaById' => $pengusahaById]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePengusahaRequest $request, $id)
    {
        //Mencari Usaha Sesuai Id Usaha
        $userById = User::find($id);

        // Update User
        $password = bcrypt(strtolower(preg_replace("/[^a-zA-Z]/", "", $request->nama)) . "123");
        $arrayUser = array(

            'nik'           => $request->nik,
            'nama'          => $request->nama,
            'jenis_kelamin' => $request->jenis_kelamin,
            'no_hp'         => $request->no_hp,
            'alamat'        => $request->alamat,
            'password'      => $password

        );
        $userById->update($arrayUser);
        return redirect()->route('pengusaha.index')->with(['message' => 'Pengusaha berhasil diupdate']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
