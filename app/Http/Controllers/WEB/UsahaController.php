<?php

namespace App\Http\Controllers\WEB;

use App\Http\Controllers\Controller;
use App\Http\Requests\Usaha\StoreUsahaRequest;
use App\Http\Requests\Usaha\UpdateUsahaRequest;
use App\Models\Foto;
use App\Models\JenisBadanUsaha;
use App\Models\Kbli;
use App\Models\ListKbli;
use App\Models\Perizinan;
use App\Models\Usaha;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsahaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $daftarUsaha = Usaha::with('user')->get();
        return view('usaha.listumkm')->with(['tittle' => 'LIST UMKM', 'daftarUsaha' => $daftarUsaha]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $listKbli = ListKbli::all();
        $daftarJenisBadanUsaha = JenisBadanUsaha::all();
        return view('usaha.create', ['tittle' => 'CREATE', 'daftarJenisBadanUsaha' => $daftarJenisBadanUsaha, 'listKbli' => $listKbli]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUsahaRequest $request)
    {

        //input user
        $user = new User;
        $user->nik            = $request->nik;
        $user->nama           = $request->nama;
        $user->jenis_kelamin  = $request->jenis_kelamin;
        $user->no_hp          = $request->no_hp;
        $user->alamat         = $request->alamat;
        $user->password       = bcrypt(strtolower(preg_replace("/[^a-zA-Z]/", "", $request->nama)) . "123");
        $user->save();

        //menetapkan role pengusaha
        $userById = User::find($user->id);
        $userById->assignRole('pengusaha');

        //input data usaha
        $usaha = new Usaha(
            [
                'nama'                  => $request->nama_usaha,
                'nib'                   => $request->nib,
                'jenis'                 => $request->jenis_usaha,
                'jenis_badan_usaha_id'  => $request->jenis_badan_usaha,
                'alamat'                => $request->alamat_usaha,
                'aset'                  => $request->aset,
                'rata_omset_perbulan'   => $request->rata_omset_perbulan,
                'karyawan_lk'           => $request->karyawan_lk,
                'karyawan_pr'           => $request->karyawan_pr,
            ]
        );
        $saveUsaha =  $userById->usaha()->save($usaha);

        //input data izin
        foreach ($request->perizinan as $izin) {
            $perizinan = new Kbli(
                [
                    'tanggal'       => $izin['tanggal'],
                    'iumk_nomor'    => $izin['iumk'],
                    'kbli_id'       => $izin['kbli_id']
                ]
            );
            $usahaById = Usaha::find($saveUsaha->id);
            $usahaById->perizinan()->save($perizinan);
        }

        // input gambar logo
        $imgLogoName = 'logo_' . $request->file('logo')->hashName();
        $request->file('logo')->storeAs('public/uploads/img', $imgLogoName);
        $gambarLogo = new Foto(
            [
                'foto'          => 'storage/uploads/img/' . $imgLogoName,
                'jenis'         => "logo",
                'nama'          => "Logo " . $saveUsaha->nama_usaha
            ]
        );
        $usahaById = Usaha::find($saveUsaha->id);
        $usahaById->gambar()->save($gambarLogo);

        //input gambar produk
        foreach ($request->file('produk') as $key => $imgProduk) {
            $imgProdukName      = 'produk_' . $imgProduk->hashName();
            $imgProduk->storeAs('public/uploads/img', $imgProdukName);
            $gambarProduk  = new Foto(
                [
                    'foto'          => 'storage/uploads/img/' . $imgProdukName,
                    'jenis'         => "produk",
                    'nama'          => "Produk " . $key + 1
                ]
            );
            $usahaById = Usaha::find($saveUsaha->id);
            $usahaById->gambar()->save($gambarProduk);
        }

        //input gambar lokasi
        foreach ($request->file('lokasi') as $key => $imgLokasi) {
            $imgLokasiName      = 'lokasi_' . $imgLokasi->hashName();
            $imgLokasi->storeAs('public/uploads/img', $imgLokasiName);
            $gambarLokasi  = new Foto(
                [
                    'foto'          => 'storage/uploads/img/' . $imgLokasiName,
                    'jenis'         => "lokasi",
                    'nama'          => "Lokasi " . $key + 1
                ]
            );
            $usahaById = Usaha::find($saveUsaha->id);
            $usahaById->gambar()->save($gambarLokasi);
        }

        return redirect()->route('usaha.create')->with(['message' => 'Usaha Baru Berhasil Diinput']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Mendapatkan daftar jenis badan usaha
        $daftarJenisBadanUsaha = JenisBadanUsaha::all();

        //Mendapatkan relasi usaha   
        $relasiUsahaById = Usaha::where('id', $id)->with('user', 'jenisBadanUsaha', 'gambar', 'perizinan', 'perizinan.kbli')->get();

        //Jika gambar tidak ada maka tampilkan gambar alternatif
        if (!empty($relasiUsahaById[0]->gambar)) {
            foreach ($relasiUsahaById[0]->gambar as $gambar) {
                //Jika logo ada, masukan ke variabel logo
                if ($gambar->jenis === 'logo') {
                    $logo = $gambar->foto;
                }



                //Jika gambar produk ada maka masukan ke array produk, jika tidak maka beri string kosong
                if ($gambar->jenis === 'produk') {
                    $produk[] = $gambar;
                }

                //Jika gambar lokasi ada maka masukan ke array produk, jika tidak maka beri string kosong
                if ($gambar->jenis === 'lokasi') {
                    $lokasi[] = $gambar;
                }
            }
        }

        $logoAlt = 'img/picture.png';
        $lokasiAlt[] = '';
        $produkAlt[] = '';

        return view('usaha.edit', ['tittle' => 'EDIT', 'daftarJenisBadanUsaha' => $daftarJenisBadanUsaha, 'relasiUsahaById' => $relasiUsahaById, "logo" => (isset($logo) ? $logo : $logoAlt), "lokasi" => (isset($lokasi) ? $lokasi : $lokasiAlt), "produk" => (isset($produk) ? $produk : $produkAlt)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUsahaRequest $request, $id)
    {
        //Mencari Usaha Sesuai Id Usaha
        $usahaById = Usaha::find($id);
        $loginUser = User::find(Auth::user()->id);

        //Update data usaha
        $arrayUsaha = array(
            'nama'                  => $request->nama_usaha,
            'nib'                  => $request->nib,
            'jenis'                 => $request->jenis_usaha,
            'jenis_badan_usaha_id'  => $request->jenis_badan_usaha,
            'alamat'                => $request->alamat_usaha,
            'aset'                  => $request->aset,
            'rata_omset_perbulan'   => $request->rata_omset_perbulan,
            'karyawan_lk'           => $request->karyawan_lk,
            'karyawan_pr'           => $request->karyawan_pr,
        );
        $usahaById->update($arrayUsaha);

        //update perizinan
        if ($request->has('perizinan')) {
            //Hapus Perizinan
            $perizinanByUsahaId = Kbli::where('usaha_id', $id)->get();
            foreach ($perizinanByUsahaId as $perizinanByUsahaId) {
                $perizinanByUsahaId->delete();
            }
            //Update data izin
            foreach ($request->perizinan as $izin) {
                $izin = new Kbli(
                    [
                        'tanggal'       => $izin['tanggal'],
                        'iumk_nomor'    => $izin['iumk'],
                        'kbli_id'       => $izin['kbli_id']
                    ]
                );
                $usahaById->perizinan()->save($izin);
            }
        }

        //Update LOGO
        if ($request->has('logo')) {
            //Hapus Gambar
            $gambarByUsahaId = Foto::where(['usaha_id' => $id, 'jenis' => 'logo'])->get();

            if ($gambarByUsahaId->all() != null) {
                $imagePath = public_path() . '/' . $gambarByUsahaId[0]->foto;
                unlink($imagePath);
                $gambarByUsahaId[0]->delete();
            }


            // Update gambar logo
            $imgLogoName = 'logo_' . $request->file('logo')->hashName();
            $request->file('logo')->storeAs('public/uploads/img', $imgLogoName);
            $gambarLogo = new Foto(
                [
                    'foto'          => 'storage/uploads/img/' . $imgLogoName,
                    'jenis'         => "logo",
                    'nama'          => "Logo " . $usahaById->nama_usaha
                ]
            );
            $usahaById->gambar()->save($gambarLogo);
        }

        //hapus gambar produk
        if ($request->has('imgproduk_id')) {
            foreach ($request->imgproduk_id as $key => $produkId) {
                $gambarById = Foto::where(['id' => $produkId, 'jenis' => 'produk'])->get();
                $imagePath = public_path() . '/' . $gambarById[0]->foto;
                unlink($imagePath);
                $gambarById[0]->delete();
            }
        }

        //Update gambar produk
        if ($request->has('produk')) {

            foreach ($request->file('produk') as $key => $imgProduk) {
                $imgProdukName      = 'produk_' . $imgProduk->hashName();
                $imgProduk->storeAs('public/uploads/img', $imgProdukName);
                $gambarProduk  = new Foto(
                    [
                        'foto'          => 'storage/uploads/img/' . $imgProdukName,
                        'jenis'         => "produk",
                        'nama'          => "Produk " . $key + 1
                    ]
                );
                $usahaById->gambar()->save($gambarProduk);
            }
        }

        //hapus gambar lokasi
        if ($request->has('imglokasi_id')) {
            foreach ($request->imglokasi_id as $key => $lokasiId) {
                $gambarById = Foto::where(['id' => $lokasiId, 'jenis' => 'lokasi'])->get();
                $imagePath = public_path() . '/' . $gambarById[0]->foto;
                unlink($imagePath);
                $gambarById[0]->delete();
            }
        }

        if ($request->has('lokasi')) {
            //Update gambar lokasi
            foreach ($request->file('lokasi') as $key => $imgLokasi) {
                $imgLokasiName      = 'lokasi_' . $imgLokasi->hashName();
                $imgLokasi->storeAs('public/uploads/img', $imgLokasiName);
                $gambarLokasi  = new Foto(
                    [
                        'foto'          => 'storage/uploads/img/' . $imgLokasiName,
                        'jenis'         => "lokasi",
                        'nama'          => "Lokasi " . $key + 1
                    ]
                );
                $usahaById->gambar()->save($gambarLokasi);
            }
        }

        if ($loginUser->hasRole('pengusaha')) {
            return redirect()->route('usaha.create')->with(['message' => 'Usaha Berhasil Di Update']);
        } else {
            return redirect()->route('usaha.index')->with(['message' => 'Usaha berhasil diupdate']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usaha = Usaha::find($id);
        $user = User::find($usaha->user_id);

        $fotoByUsahaId = Foto::where('usaha_id', $id)->get();
        foreach ($fotoByUsahaId as $foto) {
            $imagePath = public_path() . '/' . $foto->foto;
            unlink($imagePath);
        }

        $hapusSuccess = $user->delete();

        if (!$hapusSuccess) {
            return redirect()->route('usaha.index')->with(['messageError' => 'Usaha ' . $usaha->nama_usaha . ' gagal dihapus']);
        } else {
            return redirect()->route('usaha.index')->with(['message' => 'Usaha ' . $usaha->nama_usaha . ' berhasil dihapus']);
        }
    }

    public function dashboard()
    {
        return view('usaha.dashboard')->with(['tittle' => 'DASHBOARD UKM']);
    }
}
