<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WEB\SmsController;
use App\Http\Controllers\WEB\UsahaController;
use App\Http\Controllers\Web\PengusahaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');

Route::group(['middleware' => 'auth'], function () {
    Route::group(['middleware' => ['permission:kelola usaha']], function () {
        Route::get('usaha/dashboard/', [UsahaController::class, 'dashboard'])->name('usaha.dashboard');
        Route::resource('usaha', UsahaController::class);
        Route::resource('sms', SmsController::class);
        Route::resource('pengusaha', PengusahaController::class);
        Route::post('/sms/write/', [SmsController::class, 'writeSms'])->name('sms.write');
    });
    Route::resource('usaha', UsahaController::class)->only([
        'create', 'update'
    ]);
});

require __DIR__ . '/auth.php';
