<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\JenisBadanUsahaController;
use App\Http\Controllers\API\KbliController;
use App\Http\Controllers\API\PasswordController;
use App\Http\Controllers\API\SmsController;
use App\Http\Controllers\API\UsahaController;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|permission
*/

Route::post('login', [AuthController::class, 'login']);
Route::post('forgot-password', [PasswordController::class, 'forgotPassword']);
Route::post('verification-otp', [PasswordController::class, 'verificationOtp']);
Route::post('change-password', [PasswordController::class, 'changePassword']);

Route::group(['middleware' => ['jwt.verify']], function () {

    Route::group(['middleware' => ['role:pengusaha|pendata']], function () {
        //lihat kbli
        Route::get('kbli', [KbliController::class, 'index']);

        //lihat jenis badan usaha
        Route::get('jenis-badan-usaha', [JenisBadanUsahaController::class, 'index']);

        //lihat usaha by id
        Route::get('usaha-by-id/{id}', [UsahaController::class, 'usahaById']);

        //update usaha
        Route::post('usaha/{id}', [UsahaController::class, 'update']);

        //logout
        Route::post('logout', [AuthController::class, 'logout']);
    });

    Route::group(['middleware' => ['role:pendata']], function () {
        //lihat list usaha
        Route::get('usaha', [UsahaController::class, 'index']);

        //input usaha
        Route::post('usaha', [UsahaController::class, 'store']);

        //hapus usaha
        Route::delete('usaha/{id}', [UsahaController::class, 'destroy']);

        //kirim pesan masal
        Route::post('sms', [SmsController::class, 'sendSms']);
    });
    Route::get('welcome', function () {
        return view('welcome');
    });
});
